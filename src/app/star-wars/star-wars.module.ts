import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StarWarsRoutingModule } from './star-wars-routing.module';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { SwapiService } from './services/swapi/swapi.service';
import { CardComponent } from './components/list/card/card.component';

@NgModule({
  declarations: [ListComponent, DetailComponent, CardComponent],
  imports: [CommonModule, StarWarsRoutingModule],
  providers: [SwapiService],
})
export class StarWarsModule {}
