import { Component, OnInit } from '@angular/core';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { SwapiService } from '../../services/swapi/swapi.service';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  private page: number = 1;
  public characterList: any[] = [];

  constructor(private _swapi: SwapiService) {}

  ngOnInit(): void {
    this.getCharacters();
  }

  // Get Character list from service
  private getCharacters(): void {
    this._swapi
      .getPagedList(this.page)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.characterList = res.data;
        console.log(this.characterList);
      });
  }

  /**
   * @description Load more data on button click
   */
  public loadMore(): void {
    this.page++;
    this._swapi
      .getPagedList(this.page)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        this.characterList = [...this.characterList, ...res.data];
        console.log(this.characterList);
      });
  }
}
