import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { starWarsConstants } from '../../../../environments/environment';

@Injectable()
export class SwapiService {
  private baseUrl: string = starWarsConstants.baseAPI;

  constructor(private _http: HttpClient) {}

  /**
   * @description Get list of characters from SWAPI
   */
  public getPagedList(pageNo: Number = 1): Observable<any> {
    return this._http
      .get(
        `${this.baseUrl}/${starWarsConstants.people}/?${starWarsConstants.page}=${pageNo}`
      )
      .pipe(map((res: any) => ({ count: res?.count, data: res?.results })));
  }

  /**
   * @description Get details of a specific character from SWAPI via id
   */
  public getCharacterDetails(id: Number): Observable<any> {
    return this._http.get(`${this.baseUrl}/${starWarsConstants.people}/${id}`);
  }
}
