import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StarWarsModule } from './star-wars/star-wars.module';
import { DefaultViewComponent } from './views/default-view/default-view.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'star-wars',
  },
  // Eager loading
  {
    path: 'star-wars',
    component: DefaultViewComponent,
    loadChildren: () => StarWarsModule,
  },
  // Lazy loading
  {
    path: 'poke',
    component: DefaultViewComponent,
    loadChildren: () => import('./poke/poke.module').then((p) => p.PokeModule),
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'star-wars',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
