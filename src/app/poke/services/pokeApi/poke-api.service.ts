import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PokeAPIConstants } from 'src/environments/environment';

@Injectable()
export class PokeApiService {
  private baseURL: string = PokeAPIConstants.baseAPI;

  constructor(private _http: HttpClient) {}

  /**
   * @description Get List of Pokemon from PokeAPI
   */
  public getPokemonList(
    offset: Number = 0,
    limit: Number = 20
  ): Observable<any> {
    return this._http
      .get(
        `${this.baseURL}/${PokeAPIConstants.pokemon}/?${PokeAPIConstants.offset}=${offset}&${PokeAPIConstants.offset}=${limit}`
      )
      .pipe(map((res: any) => res.results));
  }

  /**
   * @description Get Specific pokemon data by id
   */
  public getPokemonData(id: Number): Observable<any> {
    return this._http.get(`${this.baseURL}/${PokeAPIConstants.pokemon}/${id}`);
  }
}
