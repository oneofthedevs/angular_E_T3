import { Component, OnInit, Self } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { PokeApiService } from '../../services/pokeApi/poke-api.service';

@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  providers: [PokeApiService],
})
export class ListComponent implements OnInit {
  pokemonList: any[] = [];
  offset: number = 0;
  constructor(@Self() private _pokeAPI: PokeApiService) {}

  ngOnInit(): void {
    this.getAllPokemons();
  }

  // Get List of pokemons
  private getAllPokemons(offset: Number = 0): void {
    this._pokeAPI
      .getPokemonList(offset)
      .pipe(untilDestroyed(this))
      .subscribe((res) => {
        console.log(res);
        this.pokemonList = [...this.pokemonList, ...res];
      });
  }

  /**
   * @description Load more data on button click
   */
  public loadMore(): void {
    this.offset += 20;
    this.getAllPokemons(this.offset);
  }
}
