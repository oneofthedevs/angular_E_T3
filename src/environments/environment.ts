// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

// All constants related to star wars API
export const starWarsConstants = {
  baseAPI: 'https://swapi.dev/api',
  page: 'page',
  people: 'people',
};

export const PokeAPIConstants = {
  baseAPI: 'https://pokeapi.co/api/v2',
  pokemon: 'pokemon',
  offset: 'offset',
  limit: 'limit',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
